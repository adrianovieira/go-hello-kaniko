FROM golang:1.15.8 AS builder

COPY hello.go .

RUN go build hello.go

FROM scratch

COPY --from=builder /go/hello /

CMD ["/hello"]
